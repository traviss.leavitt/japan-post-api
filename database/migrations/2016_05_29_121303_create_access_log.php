<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('token_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('url')->nullable();
            $table->string('ip', 50)->nullable();
            $table->timestamps();
        });

        Schema::table('access_logs', function (Blueprint $table) {
            $table->foreign('token_id')
                ->references('id')
                ->on('tokens')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_logs');
    }
}
