var gulp       = require('gulp');
var sass       = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var minifyCss  = require('gulp-minify-css');
var guppy      = require('git-guppy')(gulp);
var git        = require('gulp-git');
var fs         = require('fs');

/*
	Compiles SASS files
*/
gulp.task('main-sass', ['admin-sass'], function() {
	return gulp.src('./resources/assets/css/style.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({includePaths: ['node_modules/materialize-css/sass']}).on('error', sass.logError))
		//.pipe(minifyCss())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./public/css'));
});

/*
	Compiles Admin SASS files
*/
gulp.task('admin-sass', function() {
	return gulp.src('./resources/assets/css/admin.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		//.pipe(minifyCss())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./public/css'));
});

/*
	Copy images from build to production
*/
gulp.task('fonts', function() {
	return gulp.src('./resources/assets/css/fonts/**/*')
		.pipe(gulp.dest('./public/css/fonts'));
});

/*
	Copy DB
*/
gulp.task('backup', function() {
	return gulp.src('./database/db.sqlite3')
		.pipe(gulp.dest('./database/backups'));
});

gulp.task('sass', ['main-sass', 'admin-sass']);

/*
	Gulp Build
*/
gulp.task('build', ['sass', 'fonts']);
/*
	SASS Watch
*/
gulp.task('watch', ['sass'], function() {
	gulp.watch('./resources/assets/css/**/*', ['sass']);
});

/*
    Versioning: writes to a text file git commands for versioning
*/
gulp.task('version', function() {
    var version = [];
    var gitCommands = ['log -1 --format=%ct', 'describe --tags --long', 'log --oneline | wc -l'];

    //run through each git command and add it to the version array
    //untils we run out of commands
    runCommand(gitCommands.shift());

    function runCommand(args) {
        git.exec({args: args}, function(err, stdout) {
            version.push(stdout.trim());

            if (gitCommands.length > 0) {
                runCommand(gitCommands.shift());
            } else {
                fs.writeFileSync('./.version', version.join(','));
            }
        });
    }
});

/*
    Git Hook
*/
gulp.task('post-merge', ['version', 'build']);

gulp.task('default', ['backup', 'build']);