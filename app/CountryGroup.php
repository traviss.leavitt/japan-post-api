<?php

namespace App;

use App\BaseModel;

class CountryGroup extends BaseModel
{
    protected $table = 'country_group';
    protected $fillable = ['group_id', 'country_id'];
    protected $rules = [
    	'group_id' => 'required',
    	'country_id' => 'required'
    ];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}
