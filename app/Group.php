<?php

namespace App;

use App\BaseModel;

class Group extends BaseModel
{
    protected $fillable = ['label_en', 'label_ja', 'zone'];
    protected $rules = [
        'label_en' => 'required|max:50'
    ];

    public function countries() {
        return $this->belongsToMany('App\Country')->orderBy('name_en');
    }

    public function rates() {
        return $this->hasMany('App\Rate')->orderBy('weight');
    }

    public function getLabel() {
        return sprintf('%s Zone %d', $this->label_en, $this->zone);
    }
}
