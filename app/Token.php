<?php

namespace App;

use App\BaseModel;
use \Carbon\Carbon;
use \Firebase\JWT\JWT;

class Token extends BaseModel
{
    protected $fillable = ['user_id'];
    protected $rules = [
        'user_id' => 'required',
        'jwt' => 'required'
    ];

    public static function issue($user)
    {   
        $token = new self();
        $secret = env('APP_KEY');
        $exp = $token->exp();

        $token->user_id = $user->id;
        $token->expiration = $exp;
        $token->jwt = JWT::encode([
            'userId' => $user->id
        ], $secret);
        
        $token->save();

        return $token;
    }

    public function expirationHuman()
    {
        $exp = Carbon::createFromTimestamp($this->expiration);
        return $exp->diffForHumans();
    }

    public function exp()
    {
        $now = Carbon::now();

        return $now->addDays(30)->timestamp;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
