<?php

Route::get('/', [
    'as' => 'home',
    'uses' => 'PagesController@index'
]);

Route::get('/dashboard', [
    'as' => 'dashboard',
    'middleware' => 'auth',
    'uses' => 'DashboardController@index'
]);



Route::group(['prefix' => 'api'], function() {

    /*
     * JWT-protected Routes
    */
    Route::group(['middleware' => 'api'], function() {

        Route::get('/rate', [
            'as' => 'api.rates',
            'uses' => 'RatesController@calculate'
        ]);
        
    });

    Route::post('auth/token', 'JWTController@postToken');
    Route::get('auth/token', 'JWTController@token');

    //individual api endpoints
    Route::get('remove-country-group', [
        'as' => 'api.remove-country-group',
        'uses' => 'AdminController@removeCountryGroup'
    ]);

});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

    Route::resource('tokens', 'TokensController', ['except' => 'show']);
    Route::resource('countries', 'CountriesController', ['except' => 'show']);
    Route::resource('groups', 'GroupsController', ['except' => 'show']);
    Route::resource('rates', 'RatesController', ['only' => ['edit', 'update']]);

    //Single Routes
    Route::get('/', [
        'as' => 'admin.index',
        'uses' => 'AdminController@index'
    ]);
    Route::post('add-country-group', [
        'as' => 'admin.add-country-group',
        'uses' => 'CountryGroupController@postAdd'
    ]);

});

Route::auth();
