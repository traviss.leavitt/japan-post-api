<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Firebase\JWT\JWT;
use App\Http\Requests;
use App\Token;

use Auth;
use Gate;
use Log;


class JWTController extends Controller
{
    /**
     * Catch gets to the token url
     *
     * @return \Illuminate\Http\Response
     */
    public function token(Request $request)
    {
        return response()->json([
            'status' => 403,
            'message' => trans('auth.api.403')
        ], 403);
    }   

    /**
     * Issues a JWT Token
     *
     * @return \Illuminate\Http\Response
     */
    public function postToken(Request $request)
    {   
        $secret = env('APP_KEY');
        $credentials = $request->only('email', 'password');

        if (Auth::once($credentials)) {
            if (Gate::allows('can-use-token')) {
                return response()->json(Token::issue(Auth::user())->jwt);
            }
        }

        return response()->json([
            'status' => 401,
            'message' => trans('auth.api.401')
        ], 401);
    }
}
