<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;

use App\Http\Requests;
use App\Country;
use App\CountryGroup;
use App\Rate;
use App\Group;

use Cache;


class RatesController extends Controller
{

    /**
     * Returns a single rate
     * Required params: country iso 2 or 3 code, weight in grams
     * @return \Illuminate\Http\Response
     */
    public function calculate(Request $request)
    {
        $weight = $request->input('weight', 0);
        $country_code = $request->input('country', '');
        $return = $this->_response();
        $country_len = strlen($country_code);

        if ($country_len == 2 || $country_len == 3) {
            if (is_numeric($weight)) {
                $field = $country_len == 2 ? 'iso_2' : 'iso_3';

                $country = Country::where($field, '=', strtoupper($country_code))->first();

                if ($country) {
                    //get the all the groups this country is in
                    $country_groups = CountryGroup::where('country_id', '=', $country->id)
                        ->distinct()
                        ->get();

                    if (count($country_groups)) {
                        $rates = [];
                        
                        //we go through each group individually because it much easier to get 1 rate per group
                        //impact on the DB is minimal
                        foreach ($country_groups as $country_group) {
                            $rate = Rate::where('group_id', '=', $country_group->group_id)
                                ->where('weight', '>=', $weight)
                                ->orderBy('weight', 'asc')
                                ->first();

                            if ($rate) {
                                $rates[] = [
                                    'id' => sprintf('%s-%s', $country_group->id, strtolower(str_slug($country_group->group->getField('label')))),
                                    'slug' => strtolower(str_slug($country_group->group->label_en)),
                                    'group' => $country_group->group->getField('label'),
                                    'zone' => $country_group->group->zone,
                                    'country' => $country->getField('name'),
                                    'iso_2' => $country->iso_2,
                                    'iso_3' => $country->iso_3,
                                    'weight' => intval($weight),
                                    'rate' => intval($rate->cost),
                                    'weight_class' => intval($rate->weight)
                                ];
                            }
                        }

                        $return['code'] = 200;
                        $return['response'] = $rates;
                    } else {
                        $return['response']['message'] = trans('api.rates_not_found');
                    }
                } else {
                    $return['response']['message'] = trans('api.country_not_found');
                }
            } else {
                $return['response']['message'] = trans('api.weight_not_numeric');
            }
        } else {
            $return['response']['message'] = trans('api.country_malformed');
        }

        return response()->json($return['response'], $return['code']);
    }

    private function _response()
    {
        return [
            'code' => 400,
            'response' => [
                'message' => trans('api.general_error')
            ]
        ];
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rate = Rate::find($id);
        $groups = Group::all();


        return view('admin.rates.edit', [
            'rate' => $rate,
            'groups' => $groups
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rate = Rate::find($id);
        $validated = $rate->validate($request->all());

        if ($validated->fails()) {
            return redirect()
                ->route('admin.rates.edit', ['id' => $rate->id])
                ->withInput()
                ->with('flash', [
                    'type' => 'error',
                    'message' => $validated->errors()
                ]);
        }

        $rate->fill($request->all())->save();

        Cache::flush();

        return redirect()
            ->route('admin.index')
            ->with('flash', [
                'type' => 'success',
                'message' => trans('site.forms.messages.success')
            ]);
    }

}
