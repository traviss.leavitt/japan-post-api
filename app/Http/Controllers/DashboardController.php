<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Token;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
    	$token = Token::where('user_id', $request->user()->id)->first();

    	if (!$token) {
    		$token = Token::issue($request->user());
    	}

    	return view('dashboard.index', [
    		'token' => $token
    	]);
    }
}
