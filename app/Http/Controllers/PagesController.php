<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Firebase\JWT\JWT;
use \Carbon\Carbon;

use App\Http\Requests;
use App\Country;
use App\Token;

class PagesController extends Controller
{
    public function index(Request $request) {

    	$now = Carbon::now();
    	$samples = [];
    	$temp_token = JWT::encode([
    		'bypass' => env('APP_KEY'),
    		'exp' => $now->addSeconds(10)->timestamp
    	], env('APP_KEY'));
        $random_cnt = Country::all()->random(3);
    	
        foreach ($random_cnt as $rc) {
    		$samples[] = [
    			'code' => $rc->iso_2,
    			'weight' => rand(200, 10000)
    		];
        }

        return view('index', [
            'country' => Country::all()->random(1),
            'weight' => rand(150, 2500),
            'token' => $temp_token,
            'samples' => $samples
        ]);
    }
}
