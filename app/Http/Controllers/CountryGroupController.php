<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CountryGroup;

use Cache;
use Log;

class CountryGroupController extends Controller
{

    public function postAdd(Request $request)
    {
        $country_ids = $request->input('country_ids', []);
        $group_id = $request->input('group_id', null);

        if ($group_id) {
            foreach ($country_ids as $country_id) {
                CountryGroup::firstOrCreate([
                    'group_id' => $group_id,
                    'country_id' => $country_id
                ]);
            }
            Cache::flush();
        }
        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->authorize('can-access-admin');

        CountryGroup::destroy($id);
        Cache::flush();

        return response()->json(1);
    }
}
