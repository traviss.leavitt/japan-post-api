<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Firebase\JWT\JWT;
use App\Token;
use App\User;

class TokensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = time();
        $tokens = Token::where('expiration', '>=', $now)
            ->orderBy('expiration', 'DESC')
            ->paginate(20);

        return view('admin.tokens.index', [
            'tokens' => $tokens
        ]);
    }

    /**
     * Show the form for creating a new resource.
     
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('active', true)->get();
        return view('admin.tokens.create', [
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = User::find($request->input('user_id', null));
            
            Token::issue($user);

            return redirect()
                ->route('admin.tokens.index')
                ->with('flash', [
                    'type' => 'success',
                    'message' => trans('forms.messages.success')
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()
                ->route('admin.tokens.index')
                ->with('flash', [
                    'type' => 'error',
                    'message' => trans('forms.messages.not_found')
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Token::destroy($id);
        return response()->json(1);
    }
}
