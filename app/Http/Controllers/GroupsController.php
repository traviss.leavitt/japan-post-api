<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::paginate(25);

        return view('admin.groups.index', [
            'groups' => $groups
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new Group();
        $validated = $group->validate($request->all());

        if ($validated->fails()) {
            return redirect()
                ->route('admin.countries.create')
                ->withInput()
                ->with('flash', [
                    'type' => 'error',
                    'message' => $validated->errors()
                ]);
        }

        $group->fill($request->all())->save();

        return redirect()
            ->route('admin.groups.index')
            ->with('flash', [
                'type' => 'success',
                'message' => trans('site.forms.messages.success')
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        return view('admin.groups.edit', [
            'group' => $group
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::find($id);
        $validated = $group->validate($request->all());

        if ($validated->fails()) {
            return redirect()
                ->route('admin.groups.edit', ['id' => $group->id])
                ->withInput()
                ->with('flash', [
                    'type' => 'error',
                    'message' => $validated->errors()
                ]);
        }

        $group->fill($request->all())->save();

        return redirect()
            ->route('admin.groups.index')
            ->with('flash', [
                'type' => 'success',
                'message' => trans('site.forms.messages.success')
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::destroy($id);
        return response()->json(1);
    }
}
