<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Validator;
use Cache;
use App\Group;
use App\Country;
use App\CountryGroup;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->authorize('can-access-admin');
    }

    public function index()
    {
        $countries = Country::orderBy('name_en')->get();
        $tabs = Cache::remember('admin.group.tabs', 60, function() {
            $groups = Group::orderBy('label_en')
                ->orderBy('zone')
                ->get();
            $tabs = [];

            foreach($groups as $group) {

                $tabContent = [
                    'group_id' => $group->id,
                    'countries' => $group->countries,
                    'rates' => $group->rates
                ];

                $tabs[$group->getLabel()] = $tabContent;
            }

            return $tabs;
        });
        

        return view('admin.index', [
            'page' => 'admin',
            'tabs' => $tabs,
            'countries' => $countries
        ]);   
    }

    public function removeCountryFromGroup(Request $request, $id)
    {
        CountryGroup::find($id)->delete();
        return response()->json(1);
    }

    public function removeCountryGroup(Request $request)
    {
        $country_id = $request->query('country_id', null);
        $group_id = $request->query('group_id', null);

        if ($country_id && $group_id) {
            CountryGroup::where('country_id', '=', $country_id)
                ->where('group_id', '=', $group_id)
                ->delete();

            return response()->json(1);
        }

        return response()->json(0);
    }

}
