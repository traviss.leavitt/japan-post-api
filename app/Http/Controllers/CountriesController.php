<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Country;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::paginate(25);

        return view('admin.countries.index', [
            'countries' => $countries
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = new Country();
        $validated = $country->validate($request->all());

        if ($validated->fails()) {
            return redirect()
                ->route('admin.countries.create')
                ->withInput()
                ->with('flash', [
                    'type' => 'error',
                    'message' => $validated->errors()
                ]);
        }

        $country->fill($request->all())->save();

        return redirect()
            ->route('admin.countries.index')
            ->with('flash', [
                'type' => 'success',
                'message' => trans('site.forms.messages.success')
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);

        return view('admin.countries.edit', [
            'country' => $country
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::find($id);
        $validated = $country->validate($request->all());

        if ($validated->fails()) {
            return redirect()
                ->route('admin.countries.edit', ['id' => $country->id])
                ->withInput()
                ->with('flash', [
                    'type' => 'error',
                    'message' => $validated->errors()
                ]);
        }

        $country->fill($request->all())->save();

        return redirect()
            ->route('admin.countries.index')
            ->with('flash', [
                'type' => 'success',
                'message' => trans('site.forms.messages.success')
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::destroy($id);
        return response()->json(1);
    }
}
