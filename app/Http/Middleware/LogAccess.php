<?php

namespace App\Http\Middleware;

use Closure;
use App\AccessLog;
use App\Token;

class LogAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header();

        if (isset($header['authorization'])) {
            $authorization = $header['authorization'][0];
            $jwt = explode(' ', $authorization)[1];

            $token = Token::where('jwt', $jwt)->first();

            if ($token) {
                $access_log = new AccessLog();

                $access_log->user_id = $token->user_id;
                $access_log->ip = $request->ip();
                $access_log->token_id = $token->id;
                $access_log->url = $request->fullUrl();

                $access_log->save();
            }
        }

        return $next($request);
    }
}
