<?php

namespace App\Http\Middleware;

use Closure;
use Gate;
use Auth;

use \Firebase\JWT\JWT;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class JWTAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $header = $request->header();

        if (isset($header['authorization'])) {
            $secret = env('APP_KEY');
            $authorization = $header['authorization'][0];
            $jwt = trim(explode(' ', $authorization)[1]);

            try {
                $decoded = JWT::decode($jwt, $secret, ['HS256']);

                //bypass
                if (isset($decoded->bypass) && $decoded->bypass == $secret) {
                    return $next($request);
                } else {

                    Auth::onceUsingId($decoded->userId);

                    //check if user can consume a valid token
                    if (Gate::denies('can-use-token')) {
                        return response()->json([
                            'code' => 0,
                            'message' => trans('auth.disabled')
                        ], 500);
                    }
                }

                return $next($request);
            } catch (\Exception $e) {
                //TODO: translate
                return response()->json([
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ], 500);
            }           
        }

        return response()->json([
            'message' => trans('auth.api.401')
        ], 401);
    }
}
