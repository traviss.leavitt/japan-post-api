<?php

namespace App;

use App\BaseModel;

class Country extends BaseModel
{
    protected $fillable = ['area_ja', 'area_en', 'name_ja', 'name_en', 'japanese', 'iso_2', 'iso_3'];
}
