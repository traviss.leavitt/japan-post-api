<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Lang;

class BaseModel extends Model
{
    protected $rules = [];

    public function validate(array $data) {
        return Validator::make($data, $this->rules);
    }

    public function getField($field_name)
    {
        $lang = Lang::getLocale();
        $lang_field = sprintf('%s_%s', $field_name, $lang);

        return $this->{$lang_field};
    }
}
