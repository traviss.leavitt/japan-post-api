<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getSecret()
    {
        if (!$this->secret) {
            $this->secret = $this->generateSecret();
            $this->save();
        }

        return $this->secret;
    }

    public function generateSecret()
    {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }

    public function politeName()
    {
        return sprintf('%s, %s', $this->last_name, $this->first_name);
    }

    public function isAdmin()
    {
        return intval($this->account_type) === 3;
    }

    public function isActive()
    {
        return intval($this->active) === 1;
    }

    public function tokens()
    {
        return $this->hasMany('App\Token');
    }
}
