<?php

namespace App;

use App\BaseModel;

class AccessLog extends BaseModel
{
    protected $fillable = ['token_id', 'parameters', 'ip'];
}
	