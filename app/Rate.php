<?php

namespace App;

use App\BaseModel;

class Rate extends BaseModel
{
	protected $fillable = ['group_id', 'weight', 'cost'];
}
