@extends('layouts.master')

@section('title', trans('site.page_titles.admin.rates'))

@section('content')
<div class="row">
    <div class="col s4 offset-s4">
        <form method="post" action="{{ route('admin.rates.update', ['id' => $rate->id]) }}">
            <div class="row">
                <div class="col s12">
                    <h2>{{ trans('site.headers.admin.edit', ['type' => 'Rate', 'id' => $rate->id]) }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <label for="labelWeight" @if(!empty($rate->weight))class="active" @endif>{{ trans('site.forms.labels.weight') }}</label>
                        <select name="group_id">
                          <option value="" disabled selected>Choose Group</option>
                           @foreach($groups as $group)
                                <option value="{{ $group->id }}" @if($group->id == $rate->group_id)selected="selected" @endif>{{ $group->getField('label') }}</option>
                            @endforeach
                        </select>                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="number" name="weight" value="{{ $rate->weight }}" id="labelWeight" />
                        <label for="labelWeight" @if(!empty($rate->weight))class="active" @endif>{{ trans('site.forms.labels.weight') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="number" name="cost" value="{{ $rate->cost }}" id="labelCost" />
                        <label for="labelCost" @if(!empty($rate->cost))class="active" @endif>{{ trans('site.forms.labels.cost') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        {!! csrf_field() !!}
                        <input name="_method" type="hidden" value="PUT">
                        <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.edit') }}</button>
                        <a href="{{ route('admin.index') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection