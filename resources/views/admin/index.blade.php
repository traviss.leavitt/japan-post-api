@extends('layouts.master')

@section('title', trans('site.page_titles.admin.index'))

@section('content')
<div class="row">
    <div class="col s2">
        <ul class="simple-tabs vertical-tabs">
            <?php $i = 0; ?>
            @foreach($tabs as $group => $content)
                <li @if($i == 0) class="active"@endif>
                    <a href="#tab-{{ str_slug($group) }}">{{ $group }}</a>
                </li>
                <?php $i++; ?>
            @endforeach
        </ul>
    </div>
    <div class="col s10">
        <div class="tab-content">
            <?php $k = 0; ?>
            @foreach($tabs as $group => $content)
                <div id="tab-{{ str_slug($group) }}" class="tab @if($k == 0)active @endif" id="tab-{{ str_slug($i) }}">
                    <div class="row">
                        <div class="col s5">
                            <h3>{{ trans('site.headers.admin.rates') }}</h3>
                            <table class="striped bordered no-fill">
                                <thead>
                                    <tr>
                                        <th class="id">Id</th>
                                        <th>Weight</th>
                                        <th>Cost</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($content['rates'] as $c)
                                        <tr>
                                            <td class="id"><a href="{{ route('admin.rates.edit', ['id' => $c->id]) }}">{{ $c->id }}</a></td>
                                            <td>{{ number_format($c->weight) }}</td>
                                            <td>{{ number_format($c->cost) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col s7">
                            <h3>{{ trans('site.headers.admin.countries') }}</h3>
                            <div class="spacer">
                                @foreach($content['countries'] as $c)
                                    <span class="chip">{{ $c->getField('name') }} <a href="{{ route('api.remove-country-group') }}?country_id={{ $c->id }}&group_id={{ $c->pivot->group_id }}" class="remove-country"><i class="material-icons">close</i></a></span>
                                @endforeach
                            </div>
                            <h3>Add Country</h3>
                            <form method="post" action="{{ route('admin.add-country-group') }}">
                                <select name="country_ids[]" multiple="multiple">
                                  <option value="" disabled selected>Choose Country</option>
                                   @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->getField('name') }}</option>
                                    @endforeach
                                </select>
                                {!! csrf_field() !!}
                                <input type="hidden" name="group_id" value="{{ $content['group_id'] }}">
                                <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.add_country') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
                <?php $k++; ?>
            @endforeach
        </div>
    </div>
</div>
        
@endsection

@section('js')
    @parent
    <script>
        $(function() {            
            $('.simple-tabs').on('click', 'li', function(evt) {
                evt.preventDefault();

                var $li = $(this);
                var tab = $li.find('a').attr('href');
                var $tab = $(tab);

                if ($tab.length) {
                    //unactive all tabs in the group
                    $tab.siblings('.tab').removeClass('active');
                    $tab.addClass('active');

                    //activate link
                    $li.siblings().removeClass('active');
                    $li.addClass('active');
                }
            });

            $('.remove-country').on('click', function(evt) {
                evt.preventDefault();
                
                var $a = $(this);

                if (window.confirm('Are you sure?')) {                    

                    $.ajax({
                        url: $a.attr('href'),
                        type: 'get'
                    }) 
                        .done(function(response) {
                            if (response == 1) {
                                $a.closest('.chip').remove();
                            }
                        });
                }

            });

        });
    </script>
@endsection