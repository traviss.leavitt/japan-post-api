@extends('layouts.master')

@section('title', trans('site.page_titles.admin.groups'))

@section('content')
<div class="container">
    @include('elements.flash')
    <h1>{{ trans('site.headers.admin.groups') }}</h1>
    <table class="striped bordered">
        <thead>
            <tr>
                <th class="id">{{ trans('site.tables.headers.id') }}</th>
                <th>{{ trans('site.tables.headers.label') }}</th>
                <th class="text-center">{{ trans('site.tables.headers.zone') }}</th>
                <th class="action">
                    <a href="{{ route('admin.groups.create') }}" class="btn-floating btn-small waves-effect waves-light green"><i class="material-icons">add</i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($groups as $group)
                <tr>
                    <td class="id">{{ $group->id }}</td>
                    <td>{{ $group->getField('label') }}</td>
                    <td class="text-center">{{ $group->zone }}</td>
                    <td class="action">
                        <a href="{{ route('admin.groups.destroy', ['id' => $group->id]) }}" onClick="javascript: return window.confirm('Are you sure?');" class="btn-flat waves-effect waves-light remove-resource"><i class="material-icons">delete</i></a>  
                        <a href="{{ route('admin.groups.edit', ['id' => $group->id]) }}" class="btn-floating btn-small waves-effect waves-light orange"><i class="material-icons">mode_edit</i></a>
                    </td>            
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection