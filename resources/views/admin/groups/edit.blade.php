@extends('layouts.master')

@section('title', trans('site.page_titles.admin.groups'))

@section('content')
<div class="row">
    <div class="col s6 offset-s3">
        <form method="post" action="{{ route('admin.groups.update', ['id' => $group->id]) }}">
            <div class="row">
                <div class="col s12">
                    <h2>{{ trans('site.headers.admin.edit', ['type' => 'Group', 'id' => $group->id]) }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="label_en" value="{{ $group->label_en }}" id="labelEn" />
                        <label for="labelEn" @if(!empty($group->label_en))class="active" @endif>{{ trans('site.forms.labels.label_en') }}</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="label_ja" value="{{ $group->label_ja }}" id="labelJa" />
                        <label for="labelJa" @if(!empty($group->label_ja))class="active" @endif>{{ trans('site.forms.labels.label_ja') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="inline-radio">
                        <input name="zone" type="radio" value="1" id="zone1" @if($group->zone == 1) checked="checked"@endif/><label for="zone1">Zone 1</label>
                        <input name="zone" type="radio" value="2" id="zone2" @if($group->zone == 2) checked="checked"@endif/><label for="zone2">Zone 2</label>
                        <input name="zone" type="radio" value="3" id="zone3" @if($group->zone == 3) checked="checked"@endif/><label for="zone3">Zone 3</label>
                        <input name="zone" type="radio" value="4" id="zone4"@if($group->zone == 4) checked="checked"@endif /><label for="zone4">Zone 4</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        {!! csrf_field() !!}
                        <input name="_method" type="hidden" value="PUT">
                        <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.edit') }}</button>
                        <a href="{{ route('admin.groups.index') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection