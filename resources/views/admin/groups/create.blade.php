@extends('layouts.master')

@section('title', trans('site.page_titles.admin.groups'))

@section('content')
<div class="row">
    <div class="col s6 offset-s3">
        <form method="post" action="{{ route('admin.groups.store') }}">
            <div class="row">
                <div class="col s12">
                    <h2>{{ trans('site.headers.admin.add', ['type' => 'Group']) }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="label_en" id="labelEn" />
                        <label for="labelEn" maxlength="50" value="{{ old('label_en') }}" required="required">{{ trans('site.forms.labels.label_en') }}</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="label_ja" maxlength="50" id="labelJa" value="{{ old('label_ja') }}" />
                        <label for="labelJa">{{ trans('site.forms.labels.label_ja') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="inline-radio">
                        <input name="zone" type="radio" value="1" id="zone1" checked="checked" /><label for="zone1">Zone 1</label>
                        <input name="zone" type="radio" value="2" id="zone2" /><label for="zone2">Zone 2</label>
                        <input name="zone" type="radio" value="3" id="zone3" /><label for="zone3">Zone 3</label>
                        <input name="zone" type="radio" value="4" id="zone4" /><label for="zone4">Zone 4</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        {!! csrf_field() !!}
                        <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.create_type', ['type' => 'Group']) }}</button>
                        <a href="{{ route('admin.groups.index') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection