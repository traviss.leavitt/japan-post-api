@extends('layouts.master')

@section('title', trans('site.page_titles.admin.countries'))

@section('content')
<div class="row">
    <div class="col s6 offset-s3">
        <form method="post" action="{{ route('admin.countries.update', ['id' => $country->id]) }}">
            <div class="row">
                <div class="col s12">
                    <h2>{{ trans('site.headers.admin.edit', ['type' => 'Country', 'id' => $country->id]) }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="name_en" value="{{ $country->name_en }}" id="nameEn" />
                        <label for="nameEn" @if(!empty($country->name_en))class="active" @endif>{{ trans('site.forms.labels.name_en') }}</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="name_ja" value="{{ $country->name_ja }}" id="nameJa" />
                        <label for="nameJa" @if(!empty($country->name_ja))class="active" @endif>{{ trans('site.forms.labels.name_ja') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="area_en" value="{{ $country->area_en }}" id="areaEn" />
                        <label for="areaEn" @if(!empty($country->area_en))class="active" @endif>{{ trans('site.forms.labels.area_en') }}</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="area_ja" value="{{ $country->area_ja }}" id="areaJa" />
                        <label for="areaJa" @if(!empty($country->area_ja))class="active" @endif>{{ trans('site.forms.labels.area_ja') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="text" name="iso_2" value="{{ $country->iso_2 }}" id="idIso2" maxlength="2" />
                        <label for="idIso2" @if(!empty($country->iso_2))class="active" @endif>{{ trans('site.forms.labels.iso2') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="text" name="iso_3" value="{{ $country->iso_3 }}" id="idIso3" maxlength="3" />
                        <label for="idIso3" @if(!empty($country->iso_3))class="active" @endif>{{ trans('site.forms.labels.iso2') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="text" name="japanese" value="{{ $country->japanese }}" id="idJapanese" />
                        <label for="idJapanese" @if(!empty($country->japanese))class="active" @endif>{{ trans('site.forms.labels.japanese') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        {!! csrf_field() !!}
                        <input name="_method" type="hidden" value="PUT">
                        <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.edit') }}</button>
                        <a href="{{ route('admin.countries.index') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection