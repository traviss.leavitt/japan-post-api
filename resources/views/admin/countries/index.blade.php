@extends('layouts.master')

@section('title', trans('site.page_titles.admin.countries'))

@section('content')
<div class="container">
    @include('elements.flash')
    <h1>{{ trans('site.headers.admin.countries') }}</h1>
    <table class="striped bordered">
        <thead>
            <tr>
                <th class="id">{{ trans('site.tables.headers.id') }}</th>
                <th>{{ trans('site.tables.headers.country_name') }}</th>
                <th class="action">
                    <a href="{{ route('admin.countries.create') }}" class="btn-floating btn-small waves-effect waves-light green"><i class="material-icons">add</i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($countries as $country)
                <tr>
                    <td class="id">{{ $country->id }}</td>
                    <td>{{ $country->getField('name') }}</td>
                    <td class="action">
                        <a href="{{ route('admin.countries.destroy', ['id' => $country->id]) }}" onClick="javascript: return window.confirm('Are you sure?');" class="btn-flat waves-effect waves-light remove-resource"><i class="material-icons">delete</i></a>                
                        <a href="{{ route('admin.countries.edit', ['id' => $country->id]) }}" class="btn-floating waves-effect waves-light orange"><i class="material-icons">mode_edit</i></a>   
                    </td>            
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="text-right">
        @include('elements.pagination', ['p' => $countries])
    </div>
</div>
@endsection