@extends('layouts.master')

@section('title', trans('site.page_titles.admin.countries'))

@section('content')
<div class="row">
    <div class="col s6 offset-s3">
        <form method="post" action="{{ route('admin.countries.store') }}">
            <div class="row">
                <div class="col s12">
                    <h2>{{ trans('site.headers.admin.add', ['type' => 'Country']) }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="name_en" value="{{ old('name_en') }}" id="nameEn" />
                        <label for="nameEn">{{ trans('site.forms.labels.name_en') }}</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="name_ja" value="{{ old('name_ja') }}" id="nameJa" />
                        <label for="nameJa">{{ trans('site.forms.labels.name_ja') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="area_en" value="{{ old('area_en') }}" id="areaEn" />
                        <label for="areaEn">{{ trans('site.forms.labels.area_en') }}</label>
                    </div>
                </div>
                <div class="col s6">
                    <div class="input-field">
                        <input type="text" name="area_ja" value="{{ old('area_ja') }}" id="areaJa" />
                        <label for="areaJa">{{ trans('site.forms.labels.area_ja') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="text" name="iso_2" value="{{ old('iso_2') }}" id="idIso2" maxlength="2" />
                        <label for="idIso2">{{ trans('site.forms.labels.iso2') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="text" name="iso_3" value="{{ old('iso_3') }}" id="idIso3" maxlength="3" />
                        <label for="idIso3">{{ trans('site.forms.labels.iso2') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input type="text" name="japanese" value="{{ old('japanese') }}" id="idJapanese" />
                        <label for="idJapanese">{{ trans('site.forms.labels.japanese') }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        {!! csrf_field() !!}
                        <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.create_type', ['type' => 'Country']) }}</button>
                        <a href="{{ route('admin.countries.index') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection