@extends('layouts.master')

@section('title', trans('site.page_titles.admin.tokens'))

@section('content')
<div class="container">
    @include('elements.flash')
    <h1>{{ trans('site.headers.admin.tokens') }}</h1>
    <table class="striped bordered">
        <thead>
            <tr>
                <th class="id">{{ trans('site.tables.headers.id') }}</th>
                <th>{{ trans('site.tables.headers.user') }}</th>
                <th>{{ trans('site.tables.headers.expiration') }}</th>
                <th>{{ trans('site.tables.headers.jwt') }}</th>
                <th class="action">
                    <a href="{{ route('admin.tokens.create') }}" class="btn-floating btn-small waves-effect waves-light green"><i class="material-icons">add</i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse($tokens as $token)
                <tr>
                    <td class="id">{{ $token->id }}</td>
                    <td>{{ $token->user->politeName() }}</td>
                    <td>{{ $token->expirationHuman() }}</td>
                    <td class="text-center"><a href="#token{{ $token->id }}" class="modal-trigger"><i class="material-icons">open_in_new</i></a></td>
                    <td class="action">    
                        <a href="{{ route('admin.tokens.destroy', ['id' => $token->id]) }}" onClick="javascript: return window.confirm('Are you sure?');" type="submit" class="btn-floating btn-small waves-effect waves-light red delete-token"><i class="material-icons">delete_forever</i></a>
                        <a href="{{ route('admin.tokens.destroy', ['id' => $token->id]) }}" class="waves-effect waves-teal btn-flat"><i class="material-icons">delete</i></a>
                    </td>            
                </tr>
            @empty
                <tr>
                    <td colspan="4">
                        No Tokens Issued
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
    <div class="text-right">
        @include('elements.pagination', ['p' => $tokens])
    </div>
</div>
@foreach($tokens as $token)
<!-- Modal Structure -->
<div id="token{{ $token->id }}" class="modal">
    <div class="modal-content">
        <h4>JWT</h4>
        <p class="break">{{ $token->jwt }}</p>
    </div>
</div>
@endforeach

@endsection