@extends('layouts.master')

@section('title', trans('site.page_titles.admin.tokens'))

@section('content')
<div class="row">
    <div class="col s6 offset-s3">
        <form method="post" action="{{ route('admin.tokens.store') }}">
            <div class="row">
                <div class="col s12">
                    <h2>{{ trans('site.headers.admin.add', ['type' => 'Token']) }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <select name="user_id">
                      <option value="" disabled selected>Choose User</option>
                       @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->politeName() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        {!! csrf_field() !!}
                        <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.create_type', ['type' => 'Token']) }}</button>
                        <a href="{{ route('admin.tokens.index') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection