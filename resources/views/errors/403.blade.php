<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                background-color: #ddd;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <a href="{{ route('home') }}" class="btn-floating btn-large waves-effect waves-light yellow"><i class="material-icons">warning</i></a>
                <h1>Unauthorized</h1>                
            </div>
        </div>
    </body>
</html>
