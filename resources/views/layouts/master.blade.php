<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/admin.css') }}">
</head>
<body>
    @if(Auth::check())
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="{{ url('/logout') }}">{{ trans('site.actions.logout') }}</a></li>
        </ul>
        @if (Auth::user()->isAdmin())
            <ul id="dropdown2" class="dropdown-content">
                <li><a href="{{ route('admin.index') }}">{{ trans('site.actions.admin.index') }}</a></li>
                <li><a href="{{ route('admin.countries.index') }}">{{ trans('site.actions.admin.countries.index') }}</a></li>
                <li><a href="{{ route('admin.groups.index') }}">{{ trans('site.actions.admin.groups.index') }}</a></li>
                <li><a href="{{ route('admin.tokens.index') }}">{{ trans('site.actions.admin.tokens.index') }}</a></li>
            </ul>
        @endif
    @endif
    <nav>
        <div class="container">
            <div class="nav-wrapper">
              <a href="{{ route('home') }}" class="brand-logo left">{{ trans('site.headers.site_title') }} @include('version')</a>
              <ul id="nav-mobile" class="right hide-on-lrg-and-down">
                @if(!Auth::check())
                    <li @if($page == 'register') class="active"@endif>
                        <a href="{{ url('/register') }}">{{ trans('site.actions.register') }}</a> 
                    </li>
                @endif
               
            	@if(Auth::check())
                    @if (Auth::user()->isAdmin())
                        <li @if($page == 'admin') class="active"@endif>    
                            <a class="dropdown-button" href="#!" data-activates="dropdown2">
                                {{ trans('site.headers.admin.title') }}<i class="material-icons right">arrow_drop_down</i>
                            </a>
                        </li>
                    @endif
                    <li>    
                        <a class="dropdown-button" href="#!" data-activates="dropdown1">
                            {{ trans('site.headers.user_name', ['first_name' => Auth::user()->first_name]) }}<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                @else
                    <li @if($page == 'login') class="active"@endif>
    				    <a href="{{ url('/login') }}">{{ trans('site.actions.login') }}</a>
                    </li>
                @endif            
              </ul>
            </div>
        </div>
    </nav>

    @section('content')
    @show           

    @section('js')
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
        <script>
            var _token = '{{ csrf_token() }}';

            $(function() {
                var $doc = $(this);

                //init materialize
                $('.dropdown-button').dropdown();
                $('select').material_select();
                $('.modal-trigger').leanModal();
            
                $doc.on('click', '.remove-resource', function(evt) {
                    evt.preventDefault();

                    var $a = $(this);

                    $.ajax({
                        url: $a.attr('href'),
                        type: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': _token
                        }
                    })
                        .done(function(response) {
                            if (response == 1) {
                                $a.closest('tr').remove();
                            }
                        });
                });
            });
        </script>
    @show
</body>
</html>