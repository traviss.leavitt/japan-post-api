@extends('layouts.master')

@section('title', trans('site.page_titles.index'))

@section('content')
<div class="container">
	<div class="row">
		<div class="col s12">
			<h1>Getting Started</h1>
			<p>The current shipping methods supported: <em>EMS</em>, <em>SAL Parcel</em>, <em>Airmail</em>, and <em>SAL Small Packet</em>. <strong>This API supports only parcels</strong></p>
			<div class="divider"></div>
			<p>To get started <a href="{{ url('/register') }}">create an account</a>. This will generate a <a href="https://jwt.io/" target="_blank">JWT</a> token used to authenticate your call. There are many ways to add your token to the header. For example a way to do it with jQuery per request:</p>
			<div style="background: #f8f8f8; overflow:auto;width:auto;border:solid #d7d7d7;background-color:#f0f0f0;border-width:1px;padding:1rem;"><pre style="margin: 0; line-height: 125%"><span style="color: #008000; font-weight: bold">var</span> jwtToken <span style="color: #666666">=</span> <span style="color: #BA2121">&#39;token&#39;</span>;

$.ajax({
    url<span style="color: #666666">:</span> <span style="color: #BA2121">&#39;http://japan-post.traviss.info/api/rate&#39;</span>,
    type<span style="color: #666666">:</span> <span style="color: #BA2121">&#39;get&#39;</span>,
    data<span style="color: #666666">:</span> {
        country<span style="color: #666666">:</span> <span style="color: #BA2121">&#39;us&#39;</span>,
        weight<span style="color: #666666">:</span> <span style="color: #666666">500</span>
    },
    beforeSend<span style="color: #666666">:</span> <span style="color: #008000; font-weight: bold">function</span>(request) {
        request.setRequestHeader(<span style="color: #BA2121">&#39;Authorization&#39;</span>, <span style="color: #BA2121">&#39;Bearer &#39;</span><span style="color: #666666">+</span>jwtToken);
    }
});
	</pre></div>


			<h2>Endpoint</h2>
			 <div class="card blue-grey darken-1">
				<div class="card-content white-text">
					<span class="card-title">{{ route('api.rates') }}</span>
				</div>
			</div>
			<h2>Parameters</h2>
			<table class="bordered">
				<tr>
					<th>country</th>
					<td><a href="https://en.wikipedia.org/wiki/ISO_2" target="_blank">ISO 2</a> or <a href="https://en.wikipedia.org/wiki/ISO_3" target="_blank">ISO 3</a></td>
				</tr>
				<tr>
					<th>rate</th>
					<td>The box weight in grams</td>
				</tr>
			</table>
			<h2>Examples</h2>
			<div class="segment">
				<table class="bordered">
					<thead>
						<tr>
							<th>Country</th>
							<th>Shipping Method</th>
							<th>Weight</th>
							<th>Price</th>
						</tr>
					</thead>
					<tbody id="samples"></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	@parent

	<script>
		var _jwt_ = "{{ $token }}";
		var _samples_ = $.parseJSON('{!! json_encode($samples) !!}');

		$(function() {
			var html = '';
			var sampleLen = _samples_.length;
			var i;


			getRate(_samples_.shift());

			function getRate(data) {
				$.ajax({
					url: '/api/rate',
					type: 'get',
					data: {
						country: data.code,
						weight: data.weight
					},
					beforeSend: function(request) {
						request.setRequestHeader('Authorization', 'Bearer '+_jwt_);
					}
				})
					.done(function(response) {
						if (response) {
							$.each(response, function(i, shippingMethod) {
								html += '<tr><td>'+shippingMethod.country+'</td><td>'+shippingMethod.group+'</td><td>'+numberFormat(shippingMethod.weight)+'g</td><td>&yen;'+numberFormat(shippingMethod.rate)+'</td></tr>';
							});
						}

						if (_samples_.length > 0) {
							getRate(_samples_.shift());
						} else {
							$('#samples').html(html);
						}
					});
			}
			function numberFormat(x) {
			    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
		});
	</script>
@endsection