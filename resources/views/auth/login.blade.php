@extends('layouts.master')

@section('title', trans('site.page_titles.login'))

@section('content')

<div class="panel single login">
    <h1>{{ trans('site.headers.login') }}</h1>
    @include('elements.flash')
    <form method="post" action="{{ url('/login') }}">
        <div class="input-field">
            <input type="email" name="email" placeholder="email" required="required" value="{{ old('email') }}" />
            @if ($errors->has('email'))
                <span class="message error">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="input-field">
            <input type="password" name="password" placeholder="Password" required="required" />
        </div>
        <p>
            <input type="checkbox" name="remember" id="rememberMe" />
            <label for="rememberMe">{{ trans('site.forms.labels.remember') }}</label>
        </p>
        <div class="input-field">
            {!! csrf_field() !!}
            <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.login') }}</button>
            <a href="{{ url('/register') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.create_account') }}</a>
        </div>
    </form>
</div>

@endsection
