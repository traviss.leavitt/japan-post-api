@extends('layouts.master')

@section('title', trans('site.page_titles.register'))

@section('content')

<div class="panel single login">
    <div class="row" style="margin-bottom: 0;">
        <div class="col s12">
            <h1>{{ trans('site.headers.register') }}</h1>
        </div>
    </div>
    @include('elements.flash')
    <form method="post" action="{{ url('/register') }}">
        <div class="row">
            <div class="col s6">
                <div class="input-field">
                    <input id="idFirstName" type="text" name="first_name"value="{{ old('first_name') }}" maxlength="50" />
                    <label for="idFirstName">{{ trans('site.forms.labels.first_name') }}</label>
                    @if ($errors->has('first_name'))
                        <span class="message error">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col s6">
                <div class="input-field">
                    <input id="idLastName" type="text" name="last_name"value="{{ old('last_name') }}" maxlength="50" />
                    <label for="idLastName">{{ trans('site.forms.labels.last_name') }}</label>
                    @if ($errors->has('last_name'))
                        <span class="message error">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="input-field">
                    <input id="idEmail" type="email" name="email" required="required" />
                    <label for="idEmail">{{ trans('site.forms.labels.email') }}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <div class="input-field">
                    <input id="idPassword" type="password" name="password" maxlength="50" />
                    <label for="idPassword">{{ trans('site.forms.labels.password') }}</label>
                    @if ($errors->has('password'))
                        <span class="message error">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col s6">
                <div class="input-field">
                    <input id="idPasswordConfirmation" type="password" name="password_confirmation" maxlength="50" />
                     <label for="idPasswordConfirmation">{{ trans('site.forms.labels.password') }}</label>
                    @if ($errors->has('password_confirmation'))
                        <span class="message error">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="input-field">
                    {!! csrf_field() !!}
                    <button type="submit" class="waves-effect waves-light black btn">{{ trans('site.forms.actions.create_account') }}</button>
                    <a href="{{ url('/login') }}" class="waves-effect waves-teal btn-flat">{{ trans('site.actions.login') }}</a>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
