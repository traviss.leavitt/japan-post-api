@extends('layouts.master')

@section('title', trans('site.page_titles.dashboard'))

@section('content')
<div class="row">
	<div class="col s8 offset-s2">
		@if($token)
			<h2>{{ trans('site.headers.token') }}</h2>
			<p>{{ trans('site.token_message') }}</p>
			<div class="code">
				{{ $token->jwt }}
			</div>
		@endif
	</div>
</div>
@endsection