<?php
$version = base_path('.version');

if (file_exists($version)) {
    $file = file_get_contents($version);

    if ($file !== false) {
        $version_info = explode(',', $file);
        $last_updated = date('Y-m-d H:i', $version_info[0]);

        //chop off commit and format like so v1.1.2
        $v_arr = explode('-', $version_info[1]);
        $new_version = sprintf('%s.%s', $v_arr[0], $v_arr[1]);

        printf('<span title="Last Updated %s" style="display: inline; vertical-align: middle; background-color: transparent; color: #fff; font-size: 0.5em;">%s</span>', $last_updated, $new_version);          
    }
} 

?>