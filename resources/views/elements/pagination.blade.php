@if($p->lastPage() > 1)
<ul class="pagination">
	@if($p->currentPage() == 1)
    	<li class="disabled">
    		<a href="#!"><i class="material-icons">chevron_left</i></a>
    	</li>
    @else
    	<li class="waves-effect">
    		<a href="{{ $p->previousPageUrl() }}"><i class="material-icons">chevron_left</i></a>
    	</li>
    @endif
    @for($i = 1; $i <= $p->lastPage(); $i++)
    	@if($p->currentPage() == $i)
    		<li class="active">
    			<a href="!#">{{ $i }}</a>
    		</li>
    	@else
    		<li class="waves-effect">
    			<a href="{{ $p->url($i) }}">{{ $i }}</a>
    		</li>
    	@endif
    @endfor
    @if($p->hasMorePages())
    	<li class="waves-effect">
    		<a href="{{ $p->nextPageUrl() }}"><i class="material-icons">chevron_right</i></a>
    	</li>
    	
    @else
    	<li class="disabled">
    		<a href="#!"><i class="material-icons">chevron_right</i></a>
    	</li>
    @endif
</ul>
@endif