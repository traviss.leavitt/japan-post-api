@if (Session::has('flash'))
    <div class="message {{ Session::get('flash.type') }}">
        {{ Session::get('flash.message') }}
    </div>
@endif