<?php

return [

	'actions' => [
		'login' => 'Login',
		'logout' => 'Logout',
	    'cancel' => 'Cancel',
	    'create_account' => 'Create Account',
	    'register' => 'Register',

	    'admin' => [
	    	'index' => 'Admin',

	    	'groups' => [
	    		'index' => 'Groups'
	    	],

	    	'countries' => [
	    		'index' => 'Countries'
	    	],

	        'rates' => [
	            'index' => 'Rates'
	        ],

	        'tokens' => [
	            'index' => 'Tokens'
	        ],

	        'tabs' => [
	            'rates' => 'Rates',
	            'countries' => 'Countries'
	        ]
	    ]
	],

	'api' => [
    
	    'general_error' => 'An general error occurred',
	    'country_not_found' => 'Country not found, please use the ISO 2 or ISO 3 code',
	    'country_malformed' => 'Country code must be 2 or 3 characters in length',
	    'weight_not_numeric' => 'Weight must be a number',
	    'rates_not_found' => 'No rates found for country selected'
	    
	],


	'auth' => [

		'failed' => 'These credentials do not match our records.',
    	'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    	'need_login' => 'Please login to continue'

	],

	'forms' => [
	
		'actions' => [
			'submit' => 'Submit',
	        'edit' => 'Edit',
	        'create_account' => 'Create Account',
	        'login' => 'Login',
	        'create_type' => 'Create :type',
	        'add_country' => 'Add Country'
		],

	    'labels' => [
	        'label_en' => 'Label English',
	        'label_ja' => 'Label Japanese',
	        'zone' => 'Zone',
	        'remember' => 'Remember',
	        'first_name' => 'First Name',
	        'last_name' => 'Last Name',
	        'password' => 'Password',
	        'password_confirmation' => 'Password Confirmation',
	        'email' => 'Email',
	        'iso2' => 'ISO 2',
	        'iso3' => 'ISO 3',
	        'name_en' => 'Name English',
	        'name_ja' => 'Name Japanese',
	        'area_en' => 'Area English',
	        'area_ja' => 'Area Japanese',
	        'weight' => 'Weight',
	        'cost' => 'Cost',
	        'japanese' => 'Japanese'
	    ],

	    'placeholders' => [
	        'email' => 'Email',
	        'password' => 'Password'
	    ],

	    'messages' => [
	        'success' => 'Success'
	    ]
	],

	'headers' => [
		'site_title' => 'Japan Post API',
		'login' => 'Login',
		'register' => 'Register',
		'user_name' => 'Hello :first_name',
		'token' => 'Token',

	    'admin' => [
	        'title' => 'Admin',
	        'edit' => 'Edit :type #:id',
	        'add' => 'Add :type',
	        'groups' => 'Groups',
	        'countries' => 'Countries',
	        'rates' => 'Rates',
	        'tokens' => 'Tokens'
	    ]
	],

	'page_titles' => [

        'index' => 'Japan Post API',
        'register' => 'Register',
        'login' => 'Login',
        'dashboard' => 'Dashboard',

        'admin' => [
        	'index' => 'Japan Post API Admin',
            'groups' => 'Groups',
            'countries' => 'Countries',
            'tokens' => 'Tokens'
        ]

    ],

    'tables' => [
	
		'headers' => [
			'id' => 'Id',
			'label' => 'Label',
			'country_name' => 'Name',
			'name' => 'Name',
	        'user' => 'User',
	        'expiration' => 'Expiration',
	        'jwt' => 'JWT',
	        'zone' => 'Zone'
		]

	],

	'token_message' => 'Use the following token when making API calls.'
];